// myExemple.encrypt("123456789") ➞ "2468:<>@B"
// myExemple.encrypt("abcdefghij") ➞"bdfhjlnprt"
// myExemple.encrypt("9876543210") ➞ "::::::::::"
// myExemple.encrypt("98754310") ➞":::99988"
// myExemple.encrypt("qwerty") ➞"ryhvyu007f"
// myExemple.encrypt("azerty") ➞"b|hvyu007f"

var a = "123456789";
var b = "abcdefghij";
var c = "9876543210";
var d  ="98754310";
var e = "qwerty";
var f = "azerty";

const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
 
const encryptedStringA = cryptr.encrypt(a);
const encryptedStringB = cryptr.encrypt(b);
const encryptedStringC = cryptr.encrypt(c);
const encryptedStringD = cryptr.encrypt(d);
const encryptedStringE = cryptr.encrypt(e);
const encryptedStringF = cryptr.encrypt(f);

const decryptedStringA = cryptr.decrypt(encryptedStringA);
const decryptedStringB = cryptr.decrypt(encryptedStringB);
const decryptedStringC = cryptr.decrypt(encryptedStringC);
const decryptedStringD = cryptr.decrypt(encryptedStringD);
const decryptedStringE = cryptr.decrypt(encryptedStringE);
const decryptedStringF = cryptr.decrypt(encryptedStringF);
 
console.log(encryptedStringA);
console.log(decryptedStringA);

console.log(encryptedStringB);
console.log(decryptedStringB);

console.log(encryptedStringC);
console.log(decryptedStringC);

console.log(encryptedStringD);
console.log(decryptedStringD);

console.log(encryptedStringE);
console.log(decryptedStringE);

console.log(encryptedStringF);
console.log(decryptedStringF);